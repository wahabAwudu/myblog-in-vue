import Vue from 'vue'
import BootstrapVue from "bootstrap-vue"
import App from './App.vue'
import VueResource from 'vue-resource'
import Vuex from 'vuex'
import VueRouter from 'vue-router'
import "bootstrap/dist/css/bootstrap.min.css"
import "bootstrap-vue/dist/bootstrap-vue.css"

import Routes from './router'
import store from './store'

Vue.use(VueResource)
Vue.use(Vuex)
Vue.use(VueRouter)
Vue.use(BootstrapVue)

const router = new VueRouter({
  routes: Routes,
  mode: 'history',
})

router.beforeEach((to, from, next) => {
  if(to.meta.requiresAuth) {
    const userData = JSON.parse(localStorage.getItem('authUser'))
    // check to see if user is logged in
    if(userData && userData.token) {
      next();
    } else {
      next({ name: 'login' });
    } // end if
  }
  next();
})

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
