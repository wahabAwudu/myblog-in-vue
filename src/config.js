// base endpoint
export const baseUrl = 'http://127.0.0.1:8000/api/v1/'

// blog endpoints
export const postsUrl = baseUrl + 'posts/'
export const commentsUrl = baseUrl + 'comments/'

// auth endpoints
export const loginUrl = baseUrl + 'rest-auth/login/'
export const registerUrl = baseUrl + 'rest-auth/registration/'
export const userUrl = baseUrl + 'rest-auth/user/'
export const customUserUrl = baseUrl + 'users/'
export const refreshTokenAuth = baseUrl + 'refresh-auth-token/'
export const tokenAuth = baseUrl + 'token-auth/'

// request headers
export const getHeaders = function() {
    const userData = JSON.parse(localStorage.getItem('authUser'))
    const headers = {
        'Content-Type': 'application/json',
        'Authorization': 'JWT ' + userData.token
    }

    return headers
}
