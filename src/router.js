import Home from './components/home.vue';
import CreatePost from './components/post/form.vue';
import DetailPost from './components/post/detail.vue';
import Login from './components/auth/login.vue';
import Register from './components/auth/register.vue';
import showUser from './components/auth/showUser.vue';
import updateUser from './components/auth/updateUser.vue';

export default [
    { path: '/', redirect: '/home'},
    { path: '/home', component: Home, name: 'home', meta: { requiresAuth: true }},
    { path: '/create-post', component: CreatePost, name: 'createPost', meta: { requiresAuth: true }},
    { path: '/:id', component: DetailPost, name: 'detailPost', meta: { requiresAuth: true }},
    { path: '/user/profile', component: showUser, name: 'showUser', meta: { requiresAuth: true }},
    { path: '/user/profile/update', component: updateUser, name: 'updateUser', meta: { requiresAuth: true }},


    { path: '/access/login', component: Login, name: 'login'},
    { path: '/access/register', component: Register, name: 'register'},
]