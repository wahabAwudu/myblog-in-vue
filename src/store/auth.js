import { CURRENT_USER, LOGGED_IN } from './types'

const state = {
authUser: null,
};

const mutations = {
setAuthUser(state, userObject) {
    state.authUser = userObject;
},

clearAuthUser(state) {
    state.authUser = null;
}

};

const actions = {
    setUserObject ({commit}, userObject) {
        commit('setAuthUser', userObject);
    },
    
    clearUserObject({commit}) {
        commit('clearAuthUser');
    }
}

export default {
    // namespaced: true,
    state: state,
    mutations: mutations,
    actions: actions
}